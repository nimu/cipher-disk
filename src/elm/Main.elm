module Main exposing (Flags, Model, Msg(..), init, main, subscriptions, update, view)

-- Imports ---------------------------------------------------------------------

import Angle exposing (Angle)

import Browser 
import Html exposing (Html)
import Url exposing (Url)
import Url.Parser exposing ((</>), (<?>), Parser, parse, query, top)
import Url.Parser.Query exposing (int)
import Browser.Events exposing (onKeyPress)
import Json.Decode as Decode
import Svg.Styled exposing (Svg, circle, g, line, svg, text, text_)
import Svg.Styled.Attributes exposing (..)
import Svg.Styled.Events exposing (onClick)



-- Main ------------------------------------------------------------------------


main : Program () Model Msg
main =
    Browser.application
        { init = \flags url key -> init url
        , update = update
        , view = view
        , subscriptions = subscriptions
        , onUrlRequest = LinkClicked
        , onUrlChange = UrlChanged
        }


-- Model -----------------------------------------------------------------------


type alias Flags =
    ()


type alias Model =
    { offset : Int
    , highlight : Maybe Char
    }

type Route
    = Home
    | Rotation (Maybe Int) 
    
urlParser : Url.Parser.Parser (Route -> a) a
urlParser =
         Url.Parser.map Rotation <|
         top <?> Url.Parser.Query.int "rotation"
             
initialModel : Int -> Model
initialModel rotationValue =
    { offset = rotationValue
     , highlight = Nothing
     }
     
modelForRoute : Route -> Model
modelForRoute rotationRoute = 
    case rotationRoute of
        Home ->
            initialModel 0

        Rotation maybeInt ->
            initialModel (Maybe.withDefault 0 maybeInt)


init : Url -> ( Model, Cmd Msg )
init url =
    let
        
            rotationRoute : Route
            rotationRoute =
                case parse urlParser url of
                    Just value ->
                        value
    
                    Nothing ->
                        Rotation  (Just 0)
        in
        ( modelForRoute rotationRoute, Cmd.none )


-- Update ----------------------------------------------------------------------


type Msg
    = IncreaseAngle
    | DecreaseAngle
    | UrlChanged Url
    | LinkClicked Browser.UrlRequest
    | CharacterKey Char
    | ControlKey String


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        UrlChanged url ->
                    init url
                    
        IncreaseAngle ->
            ( { model | offset = model.offset + 1 }, Cmd.none )

        DecreaseAngle ->
            ( { model | offset = model.offset - 1 }, Cmd.none )

        CharacterKey char ->
            if Char.isAlpha char then
                ( { model | highlight = Just char }, Cmd.none )

            else
                ( model, Cmd.none )

        ControlKey _ ->
            ( model, Cmd.none )

        LinkClicked urlRequest ->
            (model, Cmd.none)
            



-- View ------------------------------------------------------------------------


outerRadius =
    180.0


middleRadius =
    140.0


innerRadius =
    100.0


xCenter =
    200.0


yCenter =
    200.0

view : Model -> Browser.Document Msg
view model =
    {
    title = "Chiffrierscheibe"
    , body = [
           Html.div []
               [ Svg.Styled.toUnstyled <| viewSvg model ] ]
    }

viewSvg : Model -> Svg Msg
viewSvg model =
    svg
        [ viewBox "0 0 400 400"
        , width "98vw"
        , height "98vh"
        , fontSize "1.5em"
        ]
    <|
        viewClearTextDisk
            ++ viewCipherDisk model.offset
            ++ viewRotationButtons
            ++ viewRotationLabel model.offset


viewRotationButtons : List (Svg Msg)
viewRotationButtons =
    [ text_
        [ x <| String.fromFloat (xCenter - 50)
        , y <| String.fromFloat yCenter
        , textAnchor "middle"
        , alignmentBaseline "middle"
        , style "font-size: 3em; cursor: pointer; user-select: none;"
        , onClick DecreaseAngle
        ]
        [ text "-" ]
    , text_
        [ x <| String.fromFloat (xCenter + 50)
        , y <| String.fromFloat yCenter
        , textAnchor "middle"
        , alignmentBaseline "central"
        , style "font-size: 3em; cursor: pointer; user-select: none;"
        , onClick IncreaseAngle
        ]
        [ text "+" ]
    ]


viewOuterDisk : String -> Svg msg
viewOuterDisk color =
    circle
        [ cx <| String.fromFloat xCenter
        , cy <| String.fromFloat yCenter
        , r <| String.fromFloat outerRadius
        , fill color
        , stroke "black"
        , strokeWidth "2"
        , class "outer-circle"
        ]
        []


viewMiddleDisk : String -> String -> Svg msg
viewMiddleDisk fillColor strokeColor =
    circle
        [ cx <| String.fromFloat xCenter
        , cy <| String.fromFloat yCenter
        , r <| String.fromFloat middleRadius
        , fill fillColor
        , stroke strokeColor
        , strokeWidth "2"
        , class "middle-circle"
        ]
        []


viewInnerDisk : String -> Svg msg
viewInnerDisk color =
    circle
        [ cx <| String.fromFloat xCenter
        , cy <| String.fromFloat yCenter
        , r <| String.fromFloat innerRadius
        , fill color
        , stroke "black"
        , strokeWidth "2"
        , class "inner-circle"
        ]
        []


viewRotationLabel : Int -> List (Svg msg)
viewRotationLabel offset =
    let
        offsetString =
            String.fromInt <| modBy 26 offset
    in
    [ text_
        [ textAnchor "middle"
        , alignmentBaseline "central"
        , x <| String.fromFloat xCenter
        , y <| String.fromFloat yCenter
        , fill "lightgray"
        , fontWeight "bolder"
        , fontSize "2em"
        , style "font-weight: bolder"
        ]
        [ text offsetString ]
    ]


viewCipherDisk : Int -> List (Svg msg)
viewCipherDisk offset =
    List.singleton <|
        g
            [ style "transition: 0.3s ease-out"

            -- , style "transform-origin: 50% 50%" -- TODO: smooth rotations, but bad letter placements
            , rotationToTransformProperty (toFloat offset / 26 |> (*) -1 |> Angle.turns) ( xCenter, yCenter )
            ]
        <|
            [ viewMiddleDisk "black" "white"
            , viewInnerDisk "white"
            ]
                ++ viewLetters innerRadius String.toUpper "white"
                ++ viewSeparators innerRadius "white"


viewSeparators : Float -> String -> List (Svg msg)
viewSeparators startingRadius color =
    List.range 65 90
        |> List.indexedMap Tuple.pair
        |> List.map Tuple.first
        |> List.map toFloat
        |> List.map (\idx -> Angle.turns (idx / 26))
        |> List.map (viewSeparator startingRadius color)


viewSeparator : Float -> String -> Angle -> Svg msg
viewSeparator startingRadius color angle =
    let
        endingRadius =
            startingRadius + 40

        startX =
            xCenter + startingRadius * cos (Angle.inRadians angle)

        startY =
            xCenter + startingRadius * sin (Angle.inRadians angle)

        endX =
            xCenter + endingRadius * cos (Angle.inRadians angle)

        endY =
            xCenter + endingRadius * sin (Angle.inRadians angle)
    in
    line
        [ x1 <| String.fromFloat startX
        , y1 <| String.fromFloat startY
        , x2 <| String.fromFloat endX
        , y2 <| String.fromFloat endY
        , stroke color
        , strokeWidth "2"
        ]
        []


viewClearTextDisk =
    [ viewOuterDisk "darkgreen"
    , viewMiddleDisk "white" "black"
    ]
        ++ viewLetters middleRadius String.toLower "white"
        ++ viewSeparators middleRadius "black"


viewLetters : Float -> (String -> String) -> String -> List (Svg msg)
viewLetters radius transformation color =
    List.range 65 90
        |> List.indexedMap Tuple.pair
        |> List.map (Tuple.mapSecond Char.fromCode)
        |> List.map (Tuple.mapSecond String.fromChar)
        |> List.map (Tuple.mapSecond transformation)
        |> List.map (viewLetter (radius + 20) color)


viewLetter : Float -> String -> ( Int, String ) -> Svg msg
viewLetter radius color ( intIndex, letter ) =
    let
        index =
            toFloat intIndex

        positionAngle =
            Angle.turns <| index / 26 - 0.25

        rotationAngle =
            Angle.turns <| index / 26

        ( xPos, yPos ) =
            ( xCenter + radius * cos (Angle.inRadians positionAngle), yCenter + radius * sin (Angle.inRadians positionAngle) )
    in
    text_
        [ textAnchor "middle"
        , alignmentBaseline "central"
        , x <| String.fromFloat xPos
        , y <| String.fromFloat yPos
        , rotationToTransformProperty rotationAngle ( xPos, yPos )
        , fill color
        ]
        [ text letter ]


rotationToTransformProperty : Angle -> ( Float, Float ) -> Svg.Styled.Attribute msg
rotationToTransformProperty angle ( xPos, yPos ) =
    transform <| "rotate(" ++ ([ Angle.inDegrees angle, xPos, yPos ] |> List.map String.fromFloat |> String.join ",") ++ ")"



-- Subscriptions ---------------------------------------------------------------


subscriptions : Model -> Sub Msg
subscriptions _ =
    Sub.batch [
        onKeyPress keyDecoder
    ]

keyDecoder : Decode.Decoder Msg
keyDecoder =
    Decode.map toKey (Decode.field "key" Decode.string)


toKey : String -> Msg
toKey keyValue =
    case String.uncons keyValue of
        Just ( char, "" ) ->
            CharacterKey char

        _ ->
            ControlKey keyValue
